import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity, Image, Alert, KeyboardAvoidingView} from 'react-native';
import ShakingText from 'react-native-shaking-text';

type Props = {};
export default class DiscountCalculatorV2 extends Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            wew:'',
            tag1:"Price",
            tag2:"Discount",
            text1:0,
            text1Count:0,
            text2:0,
            result:0,
            save:0,
            tax:false,
            counted:false,
            clearButton:false,

            inputEmpty:false,
            errorMessage:"",
            resultMessage:"",
            showText:false,
            clean:false,
        };
    }

  render() {
    return (
      <View style={{flex:1, backgroundColor:'#6490cc'}}>
        <View style={{flex:1, backgroundColor:'#67efc4', alignItems:'center'}}>
          <Text style={{marginBottom:10, textAlign:'center', fontSize:30, color:'#ffffff'}}>{this.state.wew}</Text>
            <Text style={{fontSize:15, fontWeight:'bold', marginBottom:5, color:"#ffffff"}}>{this.state.tag1}</Text>
          <View style={{height:60, width:250, backgroundColor:'#ffffff', borderRadius:20, marginBottom:20,
              shadowColor: "#000",
              shadowOffset: {
                  width: 0,
                  height: 5,
              },
              shadowOpacity: 0.36,
              shadowRadius: 6.68,

              elevation: 11,}}>
              <TextInput
                  maxLength={12}
                  placeholder={this.state.text1===0?'0':null}
                  keyboardType='numeric'
                  style={{height: 60, fontSize: 30, borderColor:'transparent', borderWidth: 1, textAlign:'center'}}
                  onChangeText={(text1) => this.setState({text1})}
                  value={this.state.text1}
                  // onBlur={()=>{this.setState({
                  //     text1:0
                  // })}}
              />
          </View>
            <Text style={{fontSize:15, fontWeight:'bold', marginBottom:5, color:"#ffffff"}}>{this.state.tag2}</Text>
          <View style={{flexDirection:'row'}}>
          <View style={{height:60, width:150, backgroundColor:'#ffffff', borderRadius:20, marginBottom:20, shadowColor: "#000",
              shadowOffset: {
                  width: 0,
                  height: 5,
              },
              shadowOpacity: 0.36,
              shadowRadius: 6.68,

              elevation: 11,}}>
              <View style={{flexDirection:'row'}}>
                <View style={{flex:0.6}}/>
              <TextInput
                  maxLength={3}
                  placeholder={this.state.text2===0?'0':null}
                  keyboardType='numeric'
                  style={{flex:0.7, height: 60, fontSize: 30, borderColor:'transparent', borderWidth: 1, textAlign:'center'}}
                  onChangeText={(text2) => this.setState({text2})}
                  value={this.state.text2!==null?this.state.text2:0}
                  // onBlur={()=>{this.setState({
                  //     text2:0
                  // })}}
              />
                  <Text style={{flex:0.6, fontSize:40, marginLeft:10}}>%</Text>
              </View>
          </View>
            {/*<Text style={{fontSize:40, marginLeft:10}}>%</Text>*/}
          </View>
            <View style={{marginBottom:20}}>
                <Text style={{fontSize:15, fontWeight:'bold', marginBottom:5, color:"#ffffff"}}>Tax Included</Text>
                <View style={{alignItems:'center'}}>
                <TouchableOpacity style={{height:25, width:25, backgroundColor:'#e4e4e4', borderRadius:50}}
                                  onPress={()=>{
                                      this.setState({
                                          tax:!this.state.tax
                                      })
                                  }}>
                    <View style={{height:18, width:18, backgroundColor:this.state.tax?'#89e600':"#e4e4e4", margin:3.8, borderRadius:50}}/>
                </TouchableOpacity>
                </View>
            </View>
          <View style={{marginBottom:5}}>
              <TouchableOpacity
                  style={{borderRadius:100, height:80, width:80, alignItems: 'center',
                      backgroundColor: '#F36F38',
                      padding: 10, activeOpacity:1, shadowColor: "#000",
                      shadowOffset: {
                          width: 0,
                          height: 4,
                      },
                      shadowOpacity: 0.30,
                      shadowRadius: 4.65,

                      elevation: 8,}}
                  onPress={()=>{
                      if(this.state.text1 === 0 && this.state.text2 === 0 ){
                          this.setState({
                              errorMessage:"",
                              tag1:"",
                              tag2:"",
                          },()=>{this.setState({
                              errorMessage:"Whoops, the price and the discount is empty",
                              tag1:"Price",
                              tag2:"Discount"
                          })})
                      }
                      if(this.state.text1 !== 0 && this.state.text2 === 0 ){
                          this.setState({
                              errorMessage:"",
                              tag2:""
                          },()=>{this.setState({
                              errorMessage:"Where's the discount?",
                              tag2:"Discount"
                          })})
                      }
                      if(this.state.text1 == 0 && this.state.text2 !== 0 ){
                          this.setState({
                              errorMessage:"",
                              tag1:"",
                          },()=>{this.setState({
                              errorMessage:"Where's the price?",
                              tag1:"Price"
                          })})
                      }
                      if(this.state.text2 >= 100 && this.state.text1 !== 0){
                          if(this.state.counted===true){
                              this.setState({
                                  showText:false,
                                  clearButton:true,
                                  result:this.state.text1,
                                  errorMessage:"",
                                  resultMessage:"",
                              },()=>{this.setState({
                                  errorMessage:"That's pretty much free, you know..."
                              })})
                          }
                          else{
                              this.setState({
                                  showText:false,
                                  clearButton:true,
                                  result:this.state.text1,
                                  errorMessage:"",
                                  resultMessage:"",
                              },()=>{this.setState({
                                  errorMessage:"That's pretty much free, you know..."
                              })})
                          }
                      }
                      if(this.state.text1 > 0 && (this.state.text2 < 100 && this.state.text2 > 0)){
                          this.calculate()
                      }
                  }}
              >
                  <Image
                      style={{height:60, width:60}}
                      source={require('discount/icons/discount.png')}
                  />
              </TouchableOpacity>
          </View>
            <View style={{alignItems:'center'}}>
                <View style={{marginVertical:10, marginBottom:10}}>
                    <Text style={{textAlign:'center', fontSize:15, fontWeight:'bold', marginBottom:5, color:"#ffffff"}}>Result</Text>
                    <View style={{height:60, width:this.state.text1.length>10?290:250, backgroundColor:'#ffffff', borderRadius:20, shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 5,
                        },
                        shadowOpacity: 0.36,
                        shadowRadius: 6.68,

                        elevation: 11,}}>
                        <Text style={{marginTop:10, textAlign:'center', fontSize:30, color:"#000000"}}>Rp.{this.state.result>0?this.state.result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","):0}</Text>
                    </View>
                </View>
                {this.state.showText === true?<Text style={{color:"#ffffff", textAlign:'center', marginBottom:10, fontSize:15, fontWeight:'bold'}}>{this.state.resultMessage}</Text>:
                    <ShakingText style={{color:"#ffffff", textAlign:'center', marginBottom:10, fontSize:15, fontWeight:'bold'}}>{this.state.errorMessage}</ShakingText>}
                {this.state.result!==0 || (this.state.result <= 0 && this.state.clearButton === true) ?
                    <TouchableOpacity
                        style={{borderRadius:100, height:60, width:60, marginBottom:10, alignItems: 'center',
                            backgroundColor: '#F36F38',
                            padding: 10, activeOpacity:1, shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 4,
                            },
                            shadowOpacity: 0.30,
                            shadowRadius: 4.65,

                            elevation: 8,}}
                        onPress={()=>
                        {this.setState({
                            text1:0,
                            text2:0,
                            result:0,
                            clearButton:false,
                            counted: false,
                            showText: false,
                            resultMessage:"",
                            errorMessage:"",
                        })}}
                    >
                        <Image
                            style={{height:40, width:40}}
                            source={require('discount/icons/reset.png')}
                        />
                    </TouchableOpacity>:null}
            </View>
        </View>
          {/*/!*Versi Aplikasi*!/*/}
          {/*<Text style={{textAlign:'center', color:"#ffffff", fontSize:13, paddingBottom:5}}>V.1.0.0</Text>*/}
      </View>
    );
  }

  async calculate(){
        if(this.state.tax!==true){
            let price = this.state.text1
            let discount = this.state.text2/100
            let discountCut = await discount*this.state.text1
            let total = await price - discountCut
            this.setState({
                counted:true,
                result:Math.round(total),
                inputEmpty:false,
                showText:true,
                resultMessage:"Great deal! you saved Rp."+Math.round(discountCut).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                errorMessage:""
            })
        }
        else{
            let price = this.state.text1
            let tax = 10/100
            let discount = this.state.text2/100
            let taxFee = await tax*this.state.text1
            let discountCut = await discount*this.state.text1
            let total = await price - discountCut
            let totalWFee = await total + taxFee
            let save = discountCut - taxFee
            this.setState({
                counted:true,
                result:Math.round(totalWFee),
                inputEmpty:false,
                showText:true,
                resultMessage:"Great deal! you saved Rp."+Math.round(save).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+" with 10% Tax applied",
                errorMessage:""

            })
        }
  }
}
