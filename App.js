import React, {Component} from 'react';
import DiscountCalculator from './discountCalculator';
import DiscountCalculatorV2 from './discountCalculatorV2';
import {createStackNavigator, createAppContainer} from 'react-navigation';

type Props = {};
export default class App extends Component<Props> {
render() {
    return (
        <DiscountCalculator/>
    );
  }
}

// const AppStackNavigator = createStackNavigator({
//     DiscountCalculator: DiscountCalculator,
//     DiscountCalculatorV2: DiscountCalculatorV2
// })
//
// const AppContainer = createAppContainer(AppStackNavigator);
